"""imagetools URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

import imagediff.views
from imagediff import views, ajax

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.start, name='start'),
    url(r'^flight/upload/$', views.flight_upload, name='upload_flight'),
    url(r'^flight/delete/$', views.flight_delete, name='delete_flight'),
    url(r'^flight/stats/show/$', views.show_flight_stats, name='show_flight_stats'),
    url(r'^diff/show/$', views.show_diff, name='show_diff'),
    url(r'^diff/process/$', views.process_diff, name='process_diff'),
    url(r'^clusters/show/$', views.show_clusters, name='show_clusters'),
    url(r'^clusters/process/$', views.process_clusters, name='process_clusters'),
    url(r'^pagegroup/show/$', views.show_page_group, name='show_page_group'),
    url(r'^version/show/$', views.show_version, name='show_version'),
    url(r'^page/edit/$', views.page_edit, name='page_edit'),
    url(r'^page/compare/$', views.page_compare, name='page_compare'),
    url(r'^page/offer/similarity$', views.show_page_offer_similarity, name='show_page_offer_similarity'),
    url(r'^page/comps/regenerate$', views.re_generate_comps, name='re_generate_comps'),
    url(r'^workareas/show/$', views.show_work_areas, name='show_work_areas'),
    url(r'^workarea/show/$', views.show_work_area, name='show_work_area'),
    url(r'^region/add/$', ajax.offer_region_add, name='offer_region_add'),
    url(r'^region/edit/$', ajax.offer_region_edit, name='offer_region_edit'),
    url(r'^region/delete/$', ajax.offer_region_delete, name='offer_region_delete'),
    url(r'^regions/get/$', ajax.offer_regions_get, name='offer_regions_get'),
    url(r'^reinit/$', views.reinit, name='reinit'),
    url(r'^img/', views.get_img, name='get_img'),
    url(r'^waimg/', views.get_work_area_img, name='get_work_area_img'),
]
