import json
import logging
import threading
from concurrent.futures.thread import ThreadPoolExecutor

import numpy as np
import os
from PIL import Image
from sklearn.cluster.affinity_propagation_ import AffinityPropagation
from sklearn.cluster.mean_shift_ import MeanShift

from imagediff.file_storage import calc_resize, COMPS_DIR_NAME, get_dirs_from, get_files_types_from, ORIGINALS_DIR_NAME
from imagediff.models import Flight, Version, Page, TC_PAGE_CLUSTERING, PC_VERSION_GROUP, PC_CLUSTER

executor = ThreadPoolExecutor(max_workers=10)
logger = logging.getLogger(__name__)


def pretty_out(f, comps):
    out = "page,v1,v2,diff\n"
    for c in comps:
        out += '{0},{1},{2},{3}\n'.format(c[0],c[1],c[2],c[3])
    f.write(out)



'''
Takes a list of file paths to images, opens each image, converts them to a 1d row of greyscale pixels and runs them through a clustering algorithm.
Returns a list of cluster numbers for each element of the input image list.
'''
def cluster_images(image_paths, resize=True):
    logger.info("clustering #images={0}".format(len(image_paths)))

    if len(image_paths) > 1:
        # resize based on first image - try to slim down to number of "features" (pixels) to make clustering reasonably fast
        iwidth_orig, iheight_orig, resize_ratio, iwidth_new, iheight_new = calc_resize(image_paths[0])

        # hack to allow background jobs
        threading.current_thread().name = 'MainThread'

        # create feature matrix
        X = np.zeros((len(image_paths), iwidth_new*iheight_new))
        for idx, ip in enumerate(image_paths):
            logger.debug('converting image {0} to feature matrix'.format(ip))
            i = Image.open(ip)
            if i.height != iheight_orig or i.width != iwidth_orig:
                raise AssertionError("image {0} is different size than {1}".format(ip, image_paths[0]))
            if resize:
                # convert to greyscale (L) and resize image
                i = i.resize((iwidth_new, iheight_new)).convert('L')
            # unravel image to a row of pixel values
            X[idx] = list(i.getdata())

        logger.debug('running clustering algorithm')
        # create clustering model and fit to feature matrix
        if len(image_paths) > 3:
            model = MeanShift()
        else:
            # for some reason MeanShift can't handle <3 samples
            model = AffinityPropagation(convergence_iter=100, damping=.9)

        # y is a list of assigned clusters for each element in image_paths
        y = model.fit_predict(X)

        logger.debug('done with clustering algorithm; y={0}'.format(str(y)))
        return y
    else:
        return [0] # the one image gets a cluster all to itself... yay!


'''
Takes a 2d array of image paths.  each row is a "group" (e.g. brochure) and each column contains images to be compared (page-sets).
Returns the assigned cluster if each image in each column.
'''
def cluster_image_groups(image_path_groups, compare_by_page_sets=True, resize=True):
    logger.info("clustering #images matrix")
    image_matrix = np.array(image_path_groups)
    num_rows = image_matrix.shape[0]
    num_cols = image_matrix.shape[1]

    if compare_by_page_sets:
        # calculate clusters for each column
        data = np.zeros((num_rows, num_cols))
        for i in range(num_cols):
            data[:, i] = cluster_images(image_matrix[:, i], resize=resize)
    else:
        res = cluster_images(image_matrix.ravel(), resize=resize)
        data = res.reshape((image_matrix.shape[0], image_matrix.shape[1]))

    return data


def create_page_clusters(flight_path, result_file_name='clusters.json', write_csvs=True):
    logger.info('creating clusters for {0}'.format(flight_path))
    try :
        # get flight
        flight_db = Flight.objects.get(working_dir_path=flight_path)

        originals_path = os.path.join(flight_path, ORIGINALS_DIR_NAME)
        comps_path = os.path.join(flight_path, COMPS_DIR_NAME)
        versions = get_dirs_from(comps_path)

        task = flight_db.start_task(TC_PAGE_CLUSTERING, PC_VERSION_GROUP)
        # first group by same number of pages
        page_num_groups = {
        }
        for v in versions:
            num_pages = len(get_files_types_from(os.path.join(comps_path, v), '.jpg'))
            if not (str(num_pages) in page_num_groups):
                page_num_groups[str(num_pages)] = []
            page_num_groups[str(num_pages)].append(v)
        # logger.debug('found {0} version groups'.format(page_num_groups))
        # update progress
        task.progress = 0.10
        task.save()

        # second, check each VG to see if there are difference in page size for any of the pages
        ctr = 0
        page_num_shape_groups = {
        }
        for k, versions in page_num_groups.items():
            logger.debug("page checking vg {0}".format(k))
            if len(versions) > 1:
                X = np.zeros((len(versions), int(k)*2))
                for i, v in enumerate(versions):
                    logger.debug("clustering vg {0} v {1}".format(k, v))
                    version_db = Version.objects.get(flight=flight_db, name=v)
                    for j, p in enumerate(version_db.pages.all()):
                        X[i, j*2] = p.orig_img_height
                        X[i, (j*2)+1] = p.orig_img_width
                # model = AffinityPropagation()
                # y = model.fit_predict(X)
                y = np.zeros(len(versions))-1
                cluster = 0

                # group all members by similarity
                for idx in range(len(versions)):
                    if y[idx] >= 0: # already in a cluster
                        continue
                    master = X[idx]
                    y[idx] = cluster
                    for jdx in range(idx+1, len(versions)):
                        if np.all(X[jdx] == master):
                            y[jdx] = cluster
                    cluster += 1

            else:
                y = [0] # dummy value to create default group

            for l, label in enumerate(y):
                grp = "{0}-{1}".format(k, int(label))
                if not grp in page_num_shape_groups:
                    page_num_shape_groups[grp] = []
                page_num_shape_groups[grp].append(versions[l])

            ctr += 1
            task.progress += 0.90 * ctr/len(page_num_groups)
            task.save()


        # if > 1 VG, try to find similar pages TODO

        version_groups = page_num_shape_groups
        logger.debug('version groups={0}'.format(version_groups))
        flight_db.finish_task(TC_PAGE_CLUSTERING, PC_VERSION_GROUP)

        # now calculate cluster data for each version_group
        task = flight_db.start_task(TC_PAGE_CLUSTERING, PC_CLUSTER)
        ctr = 0
        result = []
        for k, versions in version_groups.items():
            logger.debug('clustering version group {0}, #versions={1}'.format(k, len(versions)))
            matrix = []

            for v in versions:
                # load the matrix
                matrix.append([os.path.join(comps_path, v, f) for f in os.listdir(os.path.join(comps_path, v)) if f.endswith('.jpg')])

            if len(matrix) > 1:
                # do clustering
                clusters = cluster_image_groups(matrix, True, resize=True)
            else:
                clusters = np.zeros((1, len(matrix[0])))

            # for v in versions:
            #     matrix.append([os.path.join(originals_path, v, f) for f in os.listdir(os.path.join(originals_path, v)) if f.endswith('.jpg')])
            # # do clustering
            # clusters = cluster_image_groups(matrix, True, resize=True)

            # get image ref data
            iwidth_orig, iheight_orig, resize_ratio, iwidth_new, iheight_new = calc_resize(matrix[0][0])

            # update the database
            for v in versions:
                v_db = Version.objects.get(flight=flight_db, name=v)
                v_db.version_group = str(k)
                v_db.page_ct = clusters.shape[1]
                v_db.save()
                for i, row in enumerate(matrix):
                    for j, cell in enumerate(row):
                        p_db = Page.objects.get(comp_file_path=cell)
                        p_db.similarity_group = "{0}-{1}".format(p_db.page_set, int(clusters[i, j]))
                        p_db.save()

            ctr += 1
            task.progress += float(ctr)/len(version_groups)
            task.save()

            # result.append({
            #     'version_group': k,
            #     'versions': versions,
            #     'clusters': clusters.tolist(),
            #     'meta': {
            #         'num_images': clusters.shape[1],
            #         'num_versions': clusters.shape[0],
            #         'size': {
            #             'h': iheight_new,
            #             'w': iwidth_new,
            #         }
            #     }
            # })

        flight_db.finish_task(TC_PAGE_CLUSTERING, PC_CLUSTER)


        # write JSON
        # out_path = os.path.join(flight_path, result_file_name)
        # logger.debug('writing results to {0}'.format(out_path))
        # with open(out_path, 'w') as cluster_file:
        #     cluster_file.write(json.dumps(result))
        #     cluster_file.close()
        #
        # if write_csvs:
        #     for r in result:
        #         if r['clusters']:
        #             csv_str = ","
        #             csv_str += ",".join(['page-{0}'.format(i) for i in range(len(r['clusters']))])
        #             csv_str += '\\n'
        #             for g in range(len(r['clusters'])):
        #                 csv_str += r['versions'][g] + ','
        #                 csv_str += ",".join([str(c) for c in r['clusters'][g]])
        #                 csv_str += '\\n'
        #             out_path = os.path.join(flight_path, 'cluster_version_group_{0}.csv'.format(r['version_group']))
        #             logger.debug('writing results to {0}'.format(out_path))
        #             with open(out_path, 'w') as f:
        #                 f.write(json.dumps(csv_str))
        #                 f.close()

        logger.debug('done')
    except Flight.DoesNotExist:
        logger.exception("couldn't get flight from DB!")
    except:
        logger.exception("something choked!")



def create_page_clusters_async(flight_path, result_file_name='clusters.json', write_csvs=True):
    return executor.submit(create_page_clusters,
                           flight_path=flight_path,
                           result_file_name=result_file_name,
                           write_csvs=write_csvs,
                           )


if __name__ == '__main__':
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)

    # create_page_clusters('/Users/dan/dev/code/bonial/imagetext/temp/AceHardwareNew-2526557 2/')

    create_page_clusters('/Users/dan/dev/code/bonial/imagetext/temp/AH/')
