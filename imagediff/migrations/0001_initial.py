# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-04 07:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Flight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('working_dir_path', models.FilePathField(allow_files=False, allow_folders=True)),
                ('original_archive_path', models.FilePathField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='OfferRegion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('x', models.IntegerField()),
                ('y', models.IntegerField()),
                ('width', models.IntegerField()),
                ('height', models.IntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=75)),
                ('page_set', models.IntegerField()),
                ('similarity_group', models.CharField(max_length=75, null=True)),
                ('original_file_path', models.FilePathField(allow_files=False, allow_folders=True)),
                ('comp_file_path', models.FilePathField(allow_files=False, allow_folders=True)),
                ('phash', models.CharField(max_length=75)),
                ('orig_img_height', models.IntegerField()),
                ('orig_img_width', models.IntegerField()),
                ('comp_img_height', models.IntegerField()),
                ('comp_img_width', models.IntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='RegionDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=75)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('offer_region', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imagediff.OfferRegion')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imagediff.Page')),
            ],
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('page_ct', models.IntegerField()),
                ('version_group', models.CharField(max_length=75)),
                ('expanded_originals_path', models.FilePathField(allow_files=False, allow_folders=True, null=True)),
                ('original_file_path', models.FilePathField(null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('flight', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='versions', to='imagediff.Flight')),
            ],
        ),
        migrations.AddField(
            model_name='page',
            name='offer_regions',
            field=models.ManyToManyField(through='imagediff.RegionDetails', to='imagediff.OfferRegion'),
        ),
        migrations.AddField(
            model_name='page',
            name='version',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pages', to='imagediff.Version'),
        ),
    ]
