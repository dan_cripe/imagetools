# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-11 05:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('imagediff', '0016_auto_20170610_1943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flighttask',
            name='flight',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tasks', to='imagediff.Flight'),
        ),
    ]
