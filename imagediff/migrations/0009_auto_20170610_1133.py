# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-10 11:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('imagediff', '0008_auto_20170605_1412'),
    ]

    operations = [
        migrations.CreateModel(
            name='WorkflowTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('status', models.CharField(max_length=255)),
                ('duration_msec', models.IntegerField(null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='flight',
            name='workflow_pdf_comps',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='flight_pdf_comps', to='imagediff.WorkflowTask'),
        ),
        migrations.AddField(
            model_name='flight',
            name='workflow_pdf_expanded',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='flight_pdf_expanded', to='imagediff.WorkflowTask'),
        ),
        migrations.AddField(
            model_name='flight',
            name='workflow_pdf_page_data',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='flight_pdf_page_data', to='imagediff.WorkflowTask'),
        ),
        migrations.AddField(
            model_name='flight',
            name='workflow_pdf_total_prep',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='flight_pdf_total_prep', to='imagediff.WorkflowTask'),
        ),
        migrations.AddField(
            model_name='flight',
            name='workflow_pdf_unzipped',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='flight_pdf_unzipped', to='imagediff.WorkflowTask'),
        ),
    ]
