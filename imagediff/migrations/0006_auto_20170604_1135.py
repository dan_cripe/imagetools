# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-04 11:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('imagediff', '0005_regiondetails_area_similarity'),
    ]

    operations = [
        migrations.AlterField(
            model_name='regiondetails',
            name='offer_region',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='region_details', to='imagediff.OfferRegion'),
        ),
        migrations.AlterField(
            model_name='regiondetails',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='region_details', to='imagediff.Page'),
        ),
    ]
