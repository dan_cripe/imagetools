import datetime
import json
import logging
import zipfile
from io import BytesIO

import os
from PIL import Image
from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.files.storage import FileSystemStorage
from django.db import connection
from django.http.response import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.shortcuts import render
from django.urls.base import reverse
from django.utils import timezone
from jinja2.environment import Environment

import imagediff.file_storage
from imagediff import image_diff, page_clustering, file_storage
from imagediff.ajax import logger
from imagediff.file_storage import ORIGINALS_DIR_NAME, DIFFS_DIR_NAME, offer_region_similarity_async, \
    TARGET_COMP_PIXELS, executor, generate_comps, get_image_for_region
from imagediff.models import Flight, Page, RegionDetails, Version, FP_UNZIP_TASK_NAME, TC_FLIGHT_PREP, \
    TC_PAGE_CLUSTERING

logger = logging.getLogger(__name__)

DEFAULT_PAGE_EDIT_SIZE = 800



# Create your views here.
def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
    })
    return env



def get_pdfs(dir, recurse=True):
    pdfs = [os.path.join(dir, f) for f in os.listdir(dir) if f.endswith('.pdf')]
    if recurse:
        dirs = [d for d in os.listdir(dir) if os.path.isdir(os.path.join(dir, d))]
        for d in dirs:
            sub_pdfs = get_pdfs(os.path.join(dir, d), True)
            for p in sub_pdfs:
                pdfs.append(p)
    return pdfs



def start(request, **kwargs):
    if request.method == 'POST' and request.FILES['flightzip']:
        logger.info("starting POST for flightzip")

        '''
        Dir structure:
        - MEDIA_ROOT
        -- <working_dir_path>
        --- originals
        ---- <version dirs from zip>
        --- temp
        '''

        # unzip and store
        flightzip = request.FILES['flightzip']
        unzipped = zipfile.ZipFile(flightzip)
        zip_dir_name = unzipped.filename[0:unzipped.filename.rfind('.')]

        # create working dir
        working_dir_path = os.path.join(settings.MEDIA_ROOT, zip_dir_name)

        # allow dupes?
        if os.path.exists(working_dir_path):
            # TODO handle gracefully
            logger.warning("path already exists for this flight.  aborting.  need to handle gracefully.")
            return HttpResponseRedirect(reverse('start'))

        # create DB record if needed
        try:
            flight_db = Flight.objects.get(working_dir_path=working_dir_path)
        except Flight.DoesNotExist:
            flight_db = Flight.objects.create(
                name=request.POST['flight_name'] if 'flight_name' in request.POST else zip_dir_name,
                original_archive_path=flightzip,
                working_dir_name=zip_dir_name,
                working_dir_path=working_dir_path)

        # clean up if needed
        if os.path.isfile(os.path.join(settings.MEDIA_ROOT, unzipped.filename)):
            os.remove(os.path.join(settings.MEDIA_ROOT, unzipped.filename))
        if os.path.isdir(working_dir_path):
            imagediff.image_diff.rmdir_walk(working_dir_path)

        # wipe all former workflow steps
        flight_db.tasks.filter(taskchain_name=TC_FLIGHT_PREP).delete()
        flight_db.tasks.filter(taskchain_name=TC_PAGE_CLUSTERING).delete()


        os.mkdir(working_dir_path)

        originals_path = os.path.join(working_dir_path, ORIGINALS_DIR_NAME)
        os.mkdir(originals_path)

        temp_path = os.path.join(working_dir_path, 'temp')
        os.mkdir(temp_path)

        logger.debug("unzipping...")
        flight_db.start_task(TC_FLIGHT_PREP, FP_UNZIP_TASK_NAME)

        start = timezone.now()
        unzipped.extractall(temp_path)
        pdfs = get_pdfs(temp_path)
        for pdf in pdfs:
            os.rename(pdf, os.path.join(originals_path, os.path.split(pdf)[-1]))

        flight_db.finish_task(TC_FLIGHT_PREP, FP_UNZIP_TASK_NAME)

        logger.debug("unzipped")

        # convert PDFs to dirs
        file_storage.in_process_pdf_async(working_dir_path)

        # save zip file too
        fs = FileSystemStorage()
        fs.save(flightzip.name, flightzip)

        return HttpResponseRedirect(reverse('start'))

    elif request.method == 'GET':
        # logger.debug("starting GET for start")
        # flights = get_dirs_from(settings.MEDIA_ROOT)
        flights = Flight.objects.all()

        return render(request, 'start.html', {'flights': flights})



def flight_upload(request, **kwargs):
    return render(request, 'upload.html', {})



def flight_delete(request, **kwargs):
    if request.method == 'GET':
        try:
            logger.info('delete files for flight {0}'.format(request.GET['f'] if 'f' in request.GET else 'ERROR'))
            flight = Flight.objects.get(id=request.GET['f'])
            if os.path.isfile(flight.original_archive_path):
                os.remove(flight.original_archive_path)
            if os.path.isdir(flight.working_dir_path):
                imagediff.image_diff.rmdir_walk(flight.working_dir_path)
            logger.info('delete flight {0}'.format(flight))
            flight.delete()
        except Flight.DoesNotExist:
            logger.exception("flight not found")
    return HttpResponseRedirect(reverse('start'))



def reinit(request, **kwargs):
    if request.method == 'GET':
        flight = request.GET['f']
        working_dir_path = os.path.join(settings.MEDIA_ROOT, flight)

        flight_db = Flight.objects.get(working_dir_path=working_dir_path)

        # wipe all former workflow steps
        flight_db.tasks.filter(taskchain_name=TC_FLIGHT_PREP).delete()
        flight_db.tasks.filter(taskchain_name=TC_PAGE_CLUSTERING).delete()

        # complete unzip (we're not going to do that again)
        flight_db.start_task(TC_FLIGHT_PREP, FP_UNZIP_TASK_NAME)
        flight_db.finish_task(TC_FLIGHT_PREP, FP_UNZIP_TASK_NAME)

        file_storage.in_process_pdf(working_dir_path)
    return HttpResponseRedirect(reverse('start'))



def get_img(request, **kwargs):
    # logger.debug('getting image from {0} {1} {2}'.format(request.GET['f'], request.GET['v'], request.GET['i']))
    try:
        itype = request.GET['i'].split('.')[-1]
        if 't' in request.GET:
            img = os.path.join(settings.MEDIA_ROOT, request.GET['f'], request.GET['t'], request.GET['v'],
                               request.GET['i'])
        else:
            img = os.path.join(settings.MEDIA_ROOT, request.GET['f'], ORIGINALS_DIR_NAME, request.GET['v'],
                               request.GET['i'])

        # logger.debug('getting image {0}'.format(img))
        with open(img, "rb") as f:
            if itype == 'jpg':
                return HttpResponse(f.read(), content_type="image/jpeg")
            elif itype == 'gif':
                return HttpResponse(f.read(), content_type="image/gif")
    except IOError:
        logger.exception("error streaming file")

    red = Image.new('RGBA', (1, 1), (255, 0, 0, 0))
    response = HttpResponse(content_type="image/jpeg")
    red.save(response, "JPEG")
    return response



def process_diff(request):
    logger.debug("starting...")
    if request.method == 'GET':
        if 'flight' in request.GET:
            fname = request.GET['flight']
            un_zipped_root = os.path.join(settings.MEDIA_ROOT, fname)
            # convert
            ret = image_diff.create_diff_for_async(un_zipped_root)
            logger.info('files sent for conversion')
    return HttpResponseRedirect(reverse('start'))



def show_diff(request):
    if request.method == 'GET':
        if 'flight' in request.GET:
            fname = request.GET['flight']
            logger.info('showing flight {0}'.format(fname))
            diff_dir_path = os.path.join(settings.MEDIA_ROOT, fname, DIFFS_DIR_NAME)
            with open(os.path.join(diff_dir_path, 'diffs.json')) as manifest:
                m = json.load(manifest)
                h = None
                w = None
                for v in m:
                    if "img_size" in v:
                        h = v['img_size']['h']
                        w = v['img_size']['w']

                return render(request, 'show_diff.html', {
                    'flight': m,
                    'flight_name': fname,
                })



def process_clusters(request):
    logger.debug("starting...")
    if request.method == 'GET':
        if 'flight' in request.GET:
            fname = request.GET['flight']
            logger.info('processing clusters for flight {0}'.format(fname))
            working_path = os.path.join(settings.MEDIA_ROOT, fname)
            # process
            ret = page_clustering.create_page_clusters_async(os.path.join(working_path))
            logger.info('files sent for processing')
    return HttpResponseRedirect(reverse('start'))



def show_clusters(request):
    logger.debug("starting...")
    if request.method == 'GET':
        if 'flight' in request.GET:
            fname = request.GET['flight']
            logger.info('showing clusters for flight {0}'.format(fname))
            # cluster_file = open(os.path.join(settings.MEDIA_ROOT, fname, 'clusters.json'))
            # m = json.load(cluster_file)
            flight_db = Flight.objects.get(working_dir_name=fname)
            m = flight_db.similarity_cluster_matrix()

            return render(request, 'show_clusters.html', {
                'data': m,
                'flight_name': fname,
            })



def show_page_group(request):
    logger.debug("starting...")
    if request.method == 'GET':
        fname = request.GET['f']
        flight_db = Flight.objects.get(working_dir_name=fname)
        version_group = request.GET['vg']
        page_set = int(request.GET['ps'])
        logger.info('showing page groups for flight {0} vg {2} pg {1}'.format(fname, page_set, version_group))
        # cluster_file = open(os.path.join(settings.MEDIA_ROOT, fname, 'clusters.json'))
        # m = json.load(cluster_file)
        m = Page.objects.filter(version__version_group=version_group).filter(page_set=int(page_set)).filter(
            version__flight=flight_db).order_by('name')
        ret = {}
        for p in m:
            if not p.similarity_group in ret:
                ret[str(p.similarity_group)] = []
            ret[str(p.similarity_group)].append(p)

        return render(request, 'show_page_groups.html', {
            'sim_groups': ret,
            'f': fname,
            'vg': version_group,
            'pg': page_set,
            'display_ratio': 0.4,
        })



def show_version(request):
    logger.debug("starting...")
    if request.method == 'GET':
        flight = request.GET['f']
        version = request.GET['v']
        version_db = Version.objects.get(flight__working_dir_name=flight, name=version)
        return render(request, 'show_version.html', {
            'pages': version_db.pages.all(),
            'f': flight,
            'v': version,
            'display_ratio': 0.4,
        })



def page_edit(request):
    if request.method == 'GET':
        logger.debug("starting...")
        flight = request.GET['f']
        version = request.GET['v']
        page = request.GET['p']
        size = int(request.GET['size']) if 'size' in request.GET else DEFAULT_PAGE_EDIT_SIZE
        page_db = Page.objects.filter(version__flight__working_dir_name=flight, version__name=version, name=page)[0]

        return render(request, 'page_edit.html', {
            'f': flight,
            'v': version,
            'p': page,
            'h': size,
            'w': int(size * page_db.orig_img_width / page_db.orig_img_height),
            'ratio': float(page_db.orig_img_height / size)
        })
    return HttpResponseServerError()



def page_compare(request):
    if request.method == 'GET':
        logger.debug("starting...")
        flight = request.GET['f']
        version = request.GET['v']
        page = request.GET['p']
        float(request.GET['ratio'])
        offer_region_similarity_async(flight, page, version)
        return HttpResponseRedirect("{0}?f={1}&v={2}&p={3}".format(reverse('show_page_offer_similarity'), flight, version, page))



def show_page_offer_similarity(request):
    if request.method == 'GET':
        logger.debug("starting...")
        flight = request.GET['f']
        version = request.GET['v']
        page = request.GET['p']
        page_db = Page.get_by_flight_version_name(flight, version, page)
        peers = Page.objects.filter(page_set=page_db.page_set,
                                    version__version_group=page_db.version.version_group,
                                    version__flight__working_dir_name=flight).exclude(id=page_db.id)

        # returns list of images
        data = [p for p in peers]
        data.append(page_db)
        tgt_height = 400
        h = tgt_height
        w = tgt_height / page_db.comp_img_height * page_db.comp_img_width
        return render(request, 'show_page_offer_groups.html', {
            'master': page_db,
            'data': data,
            'f': flight,
            'v': version,
            'h': h,
            'w': w,
            'ratio': float(h / page_db.orig_img_height)
        })



def show_flight_stats(request):
    flight = request.GET['f']
    logger.debug('starting for {0}'.format(flight))
    flight_db = Flight.objects.get(id=int(flight))

    with connection.cursor() as cursor:
        cursor.execute(
        'select p.name, count(*), count(distinct(rd.offer_region_id)), count(distinct(rd.offer_region_id || "_" || rd.similarity_group)) '
        'from imagediff_regiondetails rd, imagediff_page p, imagediff_version v, imagediff_flight f '
        'where rd.page_id = p.id '
        'and p.version_id = v.id '
        'and v.flight_id = f.id '
        'and f.id={0} '
        'group by p.name'.format(flight))
        tot_offers = 0
        tot_hits = 0
        tot_offer_areas = 0
        res = []
        for row in cursor.fetchall():
            res.append(row)
            tot_offers += row[1]
            tot_offer_areas += row[2]
            tot_hits += row[3]
        res.append(['total', tot_offers, tot_offer_areas, tot_hits])
        return render(request, 'show_flight_stats.html', {
            'f': flight,
            'hit_reduction': res,
            'hit_reduction_per': int((1-tot_hits/tot_offers)*100) if tot_offers > 0 else 0,
        })



def re_generate_comps(request):
    logger.info("starting: {0}".format(request.GET))
    try:
        flight = request.GET['f']
        target_pixels = request.GET['tgt_pix'] if 'tgt_pix' in request.GET else TARGET_COMP_PIXELS
        flight_db = Flight.objects.get(id=int(flight))
        executor.submit(generate_comps, flight_path=flight_db.working_dir_path, target_image_pixels=target_pixels,
                        update_db=True)
        return HttpResponseRedirect(reverse('start'))
    except:
        logger.exception('Bamm!')
        return HttpResponseServerError()



def show_work_areas(request):
    logger.info("starting: {0}".format(request.GET))
    try:
        flight_id = request.GET['f']
        with connection.cursor() as cursor:
            cursor.execute(
                'select distinct(rd.offer_region_id), rd.similarity_group ' \
                'from imagediff_regiondetails rd, imagediff_page p, imagediff_version v, imagediff_flight f ' \
                'where rd.page_id = p.id and p.version_id = v.id and v.flight_id = f.id and f.id={0}'.format(flight_id))

            work_areas = []
            for row in cursor.fetchall():
                rd = RegionDetails.objects.filter(offer_region_id=row[0], similarity_group=row[1]).first()
                wa = {}
                wa['rd_id'] = rd.id
                wa['page'] = rd.page.name
                wa['version_group'] = rd.page.version.version_group
                wa['similarity_group'] = rd.similarity_group
                work_areas.append(wa)
            return render(request, 'show_work_areas.html', {
                'f': flight_id, 'work_areas': work_areas})

    except:
        logger.exception("DOH!")
        return HttpResponseServerError()


def show_work_area(request):
    logger.info("starting: {0}".format(request.GET))
    try:
        flight_id = request.GET['f']
        rd_id = request.GET['rd_id']
        rd = RegionDetails.objects.get(id=rd_id)
        rds = RegionDetails.objects.filter(offer_region=rd.offer_region, similarity_group=rd.similarity_group)
        wa = {}
        wa['rd_id'] = rd.id
        wa['img_h'] = rd.offer_region.height if rd.offer_region.width < 400 else rd.offer_region.height * 400/rd.offer_region.width
        wa['img_w'] = rd.offer_region.width if rd.offer_region.width < 400 else 400
        wa['page'] = rd.page.name
        wa['version_group'] = rd.page.version.version_group
        wa['similarity_group'] = rd.similarity_group
        wa['versions'] = [rd.page.version.name for rd in rds]
        return render(request, 'show_work_area.html', {
            'f': flight_id, 'wa': wa})

    except:
        logger.exception("DOH!")
        return HttpResponseServerError()


def get_work_area_img(request):
    logger.info("starting: {0}".format(request.GET))
    try:
        region_details_id = request.GET['rd']
        rd = RegionDetails.objects.get(id=region_details_id)
        with BytesIO() as output:
            with get_image_for_region(rd.page, rd.offer_region) as img:
                img.save(output, 'JPEG')
                data = output.getvalue()
                return HttpResponse(data, content_type="image/jpeg")
    except IOError:
        logger.exception("error streaming file")
        return HttpResponseServerError()
    except:
        logger.exception("DOH!")
        return HttpResponseServerError()
