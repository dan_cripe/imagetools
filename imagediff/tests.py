import logging

from django.test import TestCase

# Create your tests here.
from django.test.client import Client
from django.urls.base import reverse

logger = logging.getLogger()
logger.level = logging.DEBUG


class PreferencesTests(TestCase):

    fixtures = ['test001.json']

    def test_unlink_endpoint(self):

        client = Client()

        res = client.post(reverse('offer_region_edit'),
                          {
                              'area': ['{"id":1,"x":392,"y":449,"z":100,"height":113,"width":200}'],
                              'v': ['1170524_8PG_Memorial_Day_Nat_Event_1_24041717-041752'],
                              'f': ['AceHardwareNew-2526557 2'],
                              'p': ['page-0.jpg']
                          },
                         secure=False,
                         follow=True)

        self.assertEqual(200, res.status_code)
