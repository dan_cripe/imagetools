import json
import logging

from django.http import HttpResponse
from django.utils.translation import ugettext as _

from imagediff.models import Page, OfferRegion, RegionDetails

logger = logging.getLogger(__name__)


def offer_regions_get(request):
    logger.info("ajax: {0}".format(request.POST))
    try:
        flight = request.POST['f']
        version = request.POST['v']
        page = request.POST['p']
        ratio = float(request.POST['ratio'])
        page_db = Page.get_by_flight_version_name(flight, version, page)
        offer_regions = page_db.offer_regions.all()
        ret = [{
            'region_id': r.region_details.get(page=page_db).id,
            'x': int(r.x/ratio),
            'y': int(r.y/ratio),
            'height': int(r.height/ratio),
            'width': int(r.width/ratio),
        } for r in offer_regions]
        return AjaxMessaging.send_success_response_text("success", ret)

    except:
        logger.exception("We're all gonna die!!!!")
        return AjaxMessaging.send_error_response("error processing region update", status_code=401)



def offer_region_add(request):
    logger.info("ajax: {0}".format(request.POST))
    logger.debug('creating new master region')
    flight = request.POST['f']
    version = request.POST['v']
    page = request.POST['p']
    ratio = float(request.POST['ratio'])
    area = json.loads(request.POST['area'])
    page_db = Page.get_by_flight_version_name(flight, version, page)

    # create a new one
    offer_region = OfferRegion.objects.create(
        x=int(area['x'] * ratio),
        y=int(area['y'] * ratio),
        height=int(area['height'] * ratio),
        width=int(area['width'] * ratio),
    )
    region_details = RegionDetails.objects.create(
        page=page_db,
        offer_region=offer_region,
        status='master',
        # local_area_id=area['id'],
        local_area_id=-1,
        area_similarity=1.0,
    )
    return AjaxMessaging.send_success_response_text("created new region", data={'region_id':region_details.id})


def offer_region_edit(request):
    logger.info("ajax: {0}".format(request.POST))
    try:
        rid = request.POST['region_id']
        ratio = float(request.POST['ratio'])
        area = json.loads(request.POST['area'])

        # get region in question
        try:
            region_details = RegionDetails.objects.get(id=rid)
            offer_region = region_details.offer_region
            if region_details.status == 'master':
                logger.debug('updating master region')
                # make changes
                offer_region.x = int(area['x'] * ratio)
                offer_region.y = int(area['y'] * ratio)
                offer_region.height = int(area['height'] * ratio)
                offer_region.width = int(area['width'] * ratio)
                offer_region.save()
            else:
                logger.debug('breaking link and creating new master region')
                # create a new region and link
                offer_region = OfferRegion.objects.create(
                    x=int(area['x'] * ratio),
                    y=int(area['y'] * ratio),
                    height=int(area['height'] * ratio),
                    width=int(area['width'] * ratio),
                )
                region_details.offer_region = offer_region
                region_details.status = 'master'
                # region_details.local_area_id = area['id']
                region_details.local_area_id = -1
                region_details.area_similarity = 1.0
                region_details.save()

            return AjaxMessaging.send_success_response_text("updated region")
        except OfferRegion.DoesNotExist:
            logger.exception("is this a valid condition???")
            return AjaxMessaging.send_error_response("how did this happen?", status_code=401)
        except RegionDetails.DoesNotExist:
            logger.exception("is this a valid condition???")
            return AjaxMessaging.send_error_response("how did this happen?", status_code=401)
    except:
        logger.exception("Doooommmmmm!")
        return AjaxMessaging.send_error_response("error processing region update", status_code=401)


def offer_region_delete(request):
    logger.info("ajax: {0}".format(request.POST))
    rid = request.POST['region_id']
    logger.info("deleting offer region")
    try:
        region_details = RegionDetails.objects.get(id=rid)
        share_count = RegionDetails.objects.filter(offer_region=region_details.offer_region).count()
        region_details.delete()
        if share_count == 1:
            region_details.offer_region.delete()
    except RegionDetails.DoesNotExist:
        logger.exception("can't find region details")
        return AjaxMessaging.send_error_response("how did this happen?", status_code=401)
    return AjaxMessaging.send_success_response_text("deleted region")



class AjaxMessaging():
    @staticmethod
    def send_success_response(msg_id=None, data=None):
        response = {"result": "good"}
        if msg_id:
            response['msg'] = _(msg_id)
        if data:
            response['data'] = data
        return HttpResponse(json.dumps(response),
                            content_type="application/json",
                            status=200)

    @staticmethod
    def send_success_response_text(msg=None, data=None):
        response = {"result": "good"}
        if msg is not None:
            response['msg'] = msg
        if data is not None:
            response['data'] = data
        return HttpResponse(json.dumps(response),
                            content_type="application/json",
                            status=200)

    @staticmethod
    def send_error_response(msg, status_code=500):
        response = {"result": "bad"}
        response["msg"] = msg
        return HttpResponse(json.dumps(response),
                            content_type="application/json",
                            status=status_code)
