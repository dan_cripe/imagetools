import logging
import math
import threading
from concurrent.futures.thread import ThreadPoolExecutor

import imagehash
import numpy as np
import os
from PIL import Image
from sklearn.cluster import MeanShift
from wand.image import Image as wand_img

from imagediff.models import Flight, Version, Page, RegionDetails, FP_EXPAND_TASK_NAME, \
    FP_COMPS_TASK_NAME, FP_PAGEDATA_TASK_NAME, TC_FLIGHT_PREP

logger = logging.getLogger(__name__)
executor = ThreadPoolExecutor(max_workers=10)

ORIGINALS_DIR_NAME = 'originals'
COMPS_DIR_NAME = 'comps'
DIFFS_DIR_NAME = 'diffs'
TARGET_IMAGE_PIXELS = 100000
TARGET_COMP_PIXELS = 1000000



def get_dirs_from(path):
    return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]



def get_dir_paths_from(path):
    return [os.path.join(path, d) for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]



def get_files_from(path):
    return [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]



def get_files_types_from(path, suffix):
    return [f for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f)) and f.endswith(suffix))]



def get_files_paths_from(path, suffix):
    return [os.path.join(path, f) for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]



def get_files_paths_for_type_from(path, suffix):
    return [os.path.join(path, f) for f in os.listdir(path) if
            (os.path.isfile(os.path.join(path, f)) and f.endswith(suffix))]



def pdf_to_dir(pdf_path, out_dir, skip=True):
    try:
        logger.info("converting PDF to dir {0}".format(pdf_path))
        tgt_dir = os.path.join(out_dir, os.path.split(pdf_path)[-1].rsplit('.')[0])
        if skip and os.path.exists(tgt_dir):
            logger.debug('skipping {0}'.format(pdf_path))
            return
        os.mkdir(tgt_dir)
        img = wand_img(filename=os.path.join(pdf_path), resolution=300)
        with img.convert('JPEG') as converted:
            converted.save(filename=os.path.join(tgt_dir, 'page.jpg'))

    except:
        logger.exception('CHOKE!')



def generate_comps(flight_path, target_image_pixels=TARGET_COMP_PIXELS, update_db=False):
    logger.debug("starting for flight {0}".format(flight_path))
    try:
        originals_path = os.path.join(flight_path, ORIGINALS_DIR_NAME)
        comps_path = os.path.join(flight_path, COMPS_DIR_NAME)
        if not os.path.exists(comps_path):
            os.mkdir(comps_path)
        orig_dirs = get_dirs_from(originals_path)
        for d in orig_dirs:
            logger.debug("starting for flight {0} dir {1}".format(flight_path, d))
            comp_version_path = os.path.join(comps_path, d)
            if not os.path.exists(comp_version_path):
                os.mkdir(comp_version_path)
            for i in get_files_types_from(os.path.join(originals_path, d), '.jpg'):
                img = Image.open(os.path.join(originals_path, d, i))
                iwidth_orig, iheight_orig, resize_ratio, iwidth_new, iheight_new = calc_resize_from_image(img,
                                                                                                          target_pixel_ct=target_image_pixels)
                red = img.resize((iwidth_new, iheight_new)).convert('L')
                red_path = os.path.join(comp_version_path, i)
                red.save(red_path, 'JPEG')
                if update_db:
                    try:
                        page = Page.objects.get(comp_file_path=red_path)
                        page.comp_img_height = red.height
                        page.comp_img_width = red.width
                        page.save()
                    except Page.DoesNotExist:
                        logger.exception("Dave's not here... couldn't find file {0}".format(red_path))
        logger.debug('done generating comps for {0}'.format(flight_path))
    except:
        logger.exception('CHOKE!')



def generate_page_data(flight_path, working_dir=ORIGINALS_DIR_NAME):
    logger.debug('starting...')
    try:
        flight_db = Flight.objects.get(working_dir_path=flight_path)

        originals_root = os.path.join(flight_path, ORIGINALS_DIR_NAME)
        versions_root = os.path.join(flight_path, working_dir)
        # page_data = []
        version_dirs = get_dirs_from(versions_root)
        id_ctr = 0
        for v in version_dirs:
            logger.debug('generating page data for {0}/{1}/{2}'.format(flight_path, working_dir, v))
            opath = os.path.join(originals_root, v)
            vpath = os.path.join(versions_root, v)
            version_db, was_created = Version.objects.update_or_create(
                name=v,
                flight=flight_db,
                defaults={
                    'page_ct': -1,
                    'version_group': None,
                    'expanded_originals_path': opath,
                    'original_file_path': None})
            pg_ctr = 0
            images = get_files_types_from(vpath, '.jpg')
            for i in images:
                ipath = os.path.join(versions_root, v, i)
                img = Image.open(ipath)
                # idata = {
                #     'id': id_ctr,
                #     'version': v,
                #     'page_group': pg_ctr,
                #     'name': i,
                #     'size': {
                #         'h': img.height,
                #         'w': img.width,
                #     },
                #     'phash': str(imagehash.phash(img)),
                #     'page_cluster': -1,
                # }
                # page_data.append(idata)

                opath = os.path.join(originals_root, v, i)
                oimg = Image.open(opath)

                page_db, was_created = Page.objects.update_or_create(
                    version=version_db,
                    name=i,
                    defaults={
                        'page_set': pg_ctr,
                        'phash': str(imagehash.phash(img)),
                        'orig_img_height': oimg.height,
                        'orig_img_width': oimg.width,
                        'comp_img_height': img.height,
                        'comp_img_width': img.width,
                        'original_file_path': opath,
                        'comp_file_path': ipath,
                    })

                id_ctr += 1
                pg_ctr += 1

            version_db.page_ct = pg_ctr
            version_db.save()

        # with open(os.path.join(flight_path, 'pages.json'), 'w') as f:
        #     f.write(json.dumps(page_data))
        #     f.close()
        logger.debug('done')
    except:
        logger.exception('CHOKE!')



def in_process_pdf(flight_path, pdf_dir=ORIGINALS_DIR_NAME):
    try:
        pdf_dir_path = os.path.join(flight_path, pdf_dir)
        logger.info("converting all PDF to dirs in path {0}".format(pdf_dir_path))

        flight_db = Flight.objects.get(working_dir_path=flight_path)

        # expand from PDF
        flight_db.start_task(TC_FLIGHT_PREP, FP_EXPAND_TASK_NAME)
        pdfs = get_files_types_from(pdf_dir_path, '.pdf')
        futures = []
        for f in pdfs:
            futures.append(executor.submit(pdf_to_dir,
                                           pdf_path=os.path.join(pdf_dir_path, f),
                                           out_dir=pdf_dir_path,
                                           skip=True
                                           ))
        for f in futures:
            f.result()
        flight_db.finish_task(TC_FLIGHT_PREP, FP_EXPAND_TASK_NAME)

        flight_db.start_task(TC_FLIGHT_PREP, FP_COMPS_TASK_NAME)
        generate_comps(flight_path, TARGET_COMP_PIXELS)
        flight_db.finish_task(TC_FLIGHT_PREP, FP_COMPS_TASK_NAME)

        flight_db.start_task(TC_FLIGHT_PREP, FP_PAGEDATA_TASK_NAME)
        generate_page_data(flight_path, COMPS_DIR_NAME)
        flight_db.finish_task(TC_FLIGHT_PREP, FP_PAGEDATA_TASK_NAME)

        # with open(os.path.join(pdf_dir_path, 'expanded.json'), 'w') as f:
        #     f.write(json.dumps({'status': 'complete'}))
        #     f.close()
        logger.debug('done')


    except:
        logger.exception('CHOKE!')



def in_process_pdf_async(flight_path, pdf_dir=ORIGINALS_DIR_NAME):
    return executor.submit(in_process_pdf,
                           flight_path=flight_path,
                           pdf_dir=pdf_dir,
                           )



def calc_resize(image_path):
    iref = Image.open(image_path)
    return calc_resize_from_image(iref)



def calc_resize_from_image(iref, target_pixel_ct=TARGET_IMAGE_PIXELS):
    iwidth_orig = iref.width
    iheight_orig = iref.height
    actual_pixels = iheight_orig * iwidth_orig
    resize_ratio = math.sqrt(actual_pixels / target_pixel_ct)
    iwidth_new = int(iwidth_orig / resize_ratio)
    iheight_new = int(iheight_orig / resize_ratio)
    return iwidth_orig, iheight_orig, resize_ratio, iwidth_new, iheight_new



def resize_region_if_needed(img, target_pixel_ct=TARGET_IMAGE_PIXELS):
    input_pixel_ct = img.height * img.width
    if input_pixel_ct > (target_pixel_ct * 1.25):
        iwidth_orig, iheight_orig, resize_ratio, iwidth_new, iheight_new = calc_resize_from_image(img, target_pixel_ct)
        return img.resize((iwidth_new, iheight_new))
    else:
        return img



def get_bytes_for_regions(page, regions):
    logger.debug('starting for {0}<{1}>'.format(page, regions))
    img = Image.open(page.original_file_path)
    matrices = []
    for region in regions:
        if (region.width * region.height) < 1000:
            continue
        simg = img.crop([region.x, region.y, region.x + region.width, region.y + region.height])
        simg = resize_region_if_needed(simg, int(TARGET_IMAGE_PIXELS / 10))
        # simg.save(page.original_file_path + '.{0}.jpg'.format(region), 'JPEG')
        data = simg.convert('L').getdata()
        mat = np.array(data)
        mat.resize((simg.height, simg.width))
        matrices.append(mat)
    logger.debug('returning matrix size:{0}<{1}>'.format(len(matrices), matrices[0].shape if matrices else '0,0'))
    img.close()
    return matrices


def get_image_for_region(page, region):
    logger.debug('starting for {0}<{1}>'.format(page, region))
    img = Image.open(page.original_file_path)
    simg = img.crop([region.x, region.y, region.x + region.width, region.y + region.height])
    # simg = resize_region_if_needed(simg, int(TARGET_IMAGE_PIXELS / 10))
    return simg



def offer_region_similarity(flight, page, version):
    try:
        logger.debug('starting for {0}/{1}/{2}'.format(flight, version, page))
        page_db = Page.get_by_flight_version_name(flight, version, page)
        master_offer_regions = page_db.offer_regions.all().exclude(height=0, width=0)
        mor_bytes = get_bytes_for_regions(page_db, master_offer_regions)
        peers = Page.objects.filter(page_set=page_db.page_set,
                                    version__version_group=page_db.version.version_group,
                                    version__flight__working_dir_name=flight).exclude(id=page_db.id)

        logger.debug('{0}|{1}|{2}: starting byte similarity...'.format(flight, version, page))
        all_peer_bytes = []
        for peer in peers:
            peer_bytes = get_bytes_for_regions(peer, master_offer_regions)
            all_peer_bytes.append(peer_bytes)
            # manual similarity comparison
            for idx, mb in enumerate(mor_bytes):
                delta = np.absolute(mb - peer_bytes[idx]).sum()
                similarity = 1.0 - (delta / (256 * master_offer_regions[idx].height * master_offer_regions[idx].width))
                try:
                    rd = RegionDetails.objects.get(page=peer, offer_region=master_offer_regions[idx])
                    rd.area_similarity = similarity
                    rd.save()
                except RegionDetails.DoesNotExist:
                    RegionDetails.objects.create(
                        page=peer,
                        offer_region=master_offer_regions[idx],
                        status='linked',
                        # local_area_id=master_offer_regions[idx].region_details.get(page=page_db).local_area_id,
                        local_area_id=-1,
                        area_similarity=similarity,
                    )

        logger.debug('{0}|{1}|{2}: starting offer clustering...'.format(flight, version, page))

        # clustering algorithm
        threading.current_thread().name = 'MainThread'
        for i, mor in enumerate(master_offer_regions):
            X = np.zeros((len(peers) + 1, mor_bytes[i].shape[0] * mor_bytes[i].shape[1]))
            for j, peer_bytes in enumerate(all_peer_bytes):
                X[j, :] = peer_bytes[i].ravel()
            X[-1, :] = mor_bytes[i].ravel()
            logger.debug('running model size X={0}'.format(X.shape))
            if X.shape[0] > 3:
                model = MeanShift()
                y = model.fit_predict(X)
            else:
                # HACK due to poor clustering at small sizes
                y = np.zeros((X.shape[0],))
            logger.debug('done running model')

            for k, sim in enumerate(y[0:-1]):
                rd = RegionDetails.objects.get(page=peers[k], offer_region=mor)
                if rd.area_similarity == 1.0:
                    rd.similarity_group = 0
                else:
                    rd.similarity_group = int(sim)
                rd.save()
            rd = RegionDetails.objects.get(page=page_db, offer_region=mor)
            rd.similarity_group = int(y[-1])
            rd.save()
        logger.debug('{0}|{1}|{2}: done'.format(flight, version, page))

    except:
        logger.exception("aaaarrrggghhh!")



def offer_region_similarity_async(flight, page, version):
    return executor.submit(offer_region_similarity,
                           flight=flight,
                           page=page,
                           version=version,
                           )
