from django.db import models

# Create your models here.
from django.utils import timezone

STATUS_STARTED = 'started'
STATUS_FINISHED = 'finished'
STATUS_ERROR = 'error'

TC_FLIGHT_PREP = 'flight_prep'
FP_UNZIP_TASK_NAME = 'unzip'
FP_EXPAND_TASK_NAME = 'expand'
FP_COMPS_TASK_NAME = 'gen_comps'
FP_PAGEDATA_TASK_NAME = 'page_data'

TC_PAGE_CLUSTERING = 'page_clustering'
PC_VERSION_GROUP = 'vers_grp'
PC_CLUSTER = 'cluster'

flight_taskchains = {
    TC_FLIGHT_PREP: [{
        'version': 0,
        'steps': {
            FP_UNZIP_TASK_NAME: {
                'seq_num': 0,
            },
            FP_EXPAND_TASK_NAME: {
                'seq_num': 1,
            },
            FP_COMPS_TASK_NAME: {
                'seq_num': 2,
            },
            FP_PAGEDATA_TASK_NAME: {
                'seq_num': 3,
            },
        },
    },
    ],
    TC_PAGE_CLUSTERING: [{
        'version': 0,
        'steps': {
            PC_VERSION_GROUP: {
                'seq_num': 0,
            },
            PC_CLUSTER: {
                'seq_num': 1,
            },
        },
    },
    ]

}



class Flight(models.Model):
    name = models.CharField(max_length=255)
    working_dir_name = models.CharField(max_length=255)
    working_dir_path = models.FilePathField(allow_folders=True, allow_files=False)
    original_archive_path = models.FilePathField(allow_folders=False, allow_files=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def similarity_cluster_matrix(self):
        vgs = self.versions.values_list('version_group', flat=True).distinct()
        ret = []
        for vg in vgs:
            vs = self.versions.filter(version_group=vg).order_by('name')
            page_ct = vs[0].pages.count()
            ref_page = vs[0].pages.first()
            clusters = []
            for v in vs:
                clusters.append(
                    [{'sim_grp': p.similarity_group, 'fname': p.name} for p in v.pages.all().order_by('name')])

            ret.append({
                'version_group': vg,
                'versions': [v.name for v in vs],
                'clusters': clusters,
                'meta': {
                    'num_images': page_ct,
                    'num_versions': len(vs),
                    'size': {
                        'h': ref_page.comp_img_height,
                        'w': ref_page.comp_img_width,
                    }
                }}
            )
        return ret


    def start_task(self, tc, name):
        task_db, created = FlightTask.objects.update_or_create(
            flight=self,
            taskchain_name=tc,
            # taskchain_version=flight_taskchains[tc][-1]['version'],
            seq_num=flight_taskchains[tc][-1]['steps'][name]['seq_num'],
            name=name,
            defaults={
                'status': STATUS_STARTED,
                'started': timezone.now(),
                'duration_sec': 0.0,
                'progress': 0.0,
            }
        )
        return task_db


    def finish_task(self, tc, name):
        task_db = FlightTask.objects.get(
            flight=self,
            taskchain_name=tc,
            # taskchain_version=flight_taskchains[tc][-1]['version'],
            seq_num=flight_taskchains[tc][-1]['steps'][name]['seq_num'],
            name=name,
        )
        task_db.status = STATUS_FINISHED
        task_db.duration_sec = timedelta_seconds(timezone.now() - task_db.started)
        task_db.progress = 1.0
        task_db.save()
        return task_db


    def error_task(self, tc, name):
        task_db = FlightTask.objects.get(
            flight=self,
            taskchain_name=tc,
            # taskchain_version=flight_taskchains[tc][-1]['version'],
            seq_num=flight_taskchains[tc][-1]['steps'][name]['seq_num'],
            name=name,
        )
        task_db.status = STATUS_ERROR
        task_db.duration_sec = timedelta_seconds(timezone.now() - task_db.started)
        task_db.progress = -1.0
        task_db.save()
        return task_db


    def get_current_task(self, tc):
        fts = FlightTask.objects.filter(
            flight=self,
            taskchain_name=tc,
            # taskchain_version=flight_taskchains[tc][-1]['version'],
        ).order_by('seq_num')
        return fts.last()


    def did_taskchain_fail(self, tc):
        # check if any errors
        if FlightTask.objects.filter(
                flight=self,
                taskchain_name=tc,
                # taskchain_version=flight_taskchains[tc][-1]['version'],
                status=STATUS_ERROR,
        ).count() > 0:
            return True
        else:
            return False


    def is_taskchain_in_progress(self, tc):
        # no errors
        if self.did_taskchain_fail(tc):
            return False
        else:
            # less than all tasks
            tasks = FlightTask.objects.filter(
                flight=self,
                taskchain_name=tc,
                # taskchain_version=flight_taskchains[tc][-1]['version'],
                status=STATUS_FINISHED,
            ).count()
            if tasks == 0:
                return False
            elif tasks < len(flight_taskchains[tc][-1]['steps']):
                return True

        return False


    def is_taskchain_complete(self, tc):
        # check if any errors
        if self.did_taskchain_fail(tc):
            return True
        else:
            # check if total tasks are done
            if FlightTask.objects.filter(
                    flight=self,
                    taskchain_name=tc,
                    # taskchain_version=flight_taskchains[tc][-1]['version'],
                    status=STATUS_FINISHED,
            ).count() == len(flight_taskchains[tc][-1]['steps']):
                return True

        return False


    def __str__(self):
        return "{0}: {1}".format(self.id, self.name)



class FlightTask(models.Model):
    name = models.CharField(max_length=255)
    taskchain_version = models.IntegerField(default=-1)
    taskchain_name = models.CharField(max_length=255)
    seq_num = models.IntegerField()
    status = models.CharField(max_length=255)
    progress = models.FloatField(default=0.0)
    started = models.DateTimeField(blank=True)
    duration_sec = models.IntegerField(null=True)
    flight = models.ForeignKey(Flight, related_name='tasks', on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def __str__(self):
        return "{0}: {1}|{2} ({3})".format(self.id, self.name, self.status, self.progress)



class Version(models.Model):
    name = models.CharField(max_length=255)
    page_ct = models.IntegerField()
    version_group = models.CharField(max_length=75, null=True)
    flight = models.ForeignKey(Flight, related_name='versions', on_delete=models.CASCADE, null=False)
    expanded_originals_path = models.FilePathField(allow_folders=True, allow_files=False, null=True)
    original_file_path = models.FilePathField(allow_folders=False, allow_files=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def __str__(self):
        return "{0}: {1}".format(self.id, self.name)



class OfferRegion(models.Model):
    x = models.IntegerField()
    y = models.IntegerField()
    width = models.IntegerField()
    height = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def __str__(self):
        return "{0}: {1}|{2}".format(self.id, self.x, self.y)



class Page(models.Model):
    name = models.CharField(max_length=75)
    page_set = models.IntegerField()
    similarity_group = models.CharField(max_length=75, null=True)
    version = models.ForeignKey(Version, related_name='pages', on_delete=models.CASCADE)
    offer_regions = models.ManyToManyField(OfferRegion, through='RegionDetails')
    original_file_path = models.FilePathField(allow_folders=True, allow_files=False)
    comp_file_path = models.FilePathField(allow_folders=True, allow_files=False)
    phash = models.CharField(max_length=75)
    orig_img_height = models.IntegerField()
    orig_img_width = models.IntegerField()
    comp_img_height = models.IntegerField()
    comp_img_width = models.IntegerField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    @staticmethod
    def get_by_flight_version_name(flight, version, name):
        return \
            Page.objects.filter(version__flight__working_dir_name=flight).filter(version__name=version).filter(
                name=name)[0]


    def __str__(self):
        return "{0}: {1}".format(self.id, self.name)



class RegionDetails(models.Model):
    page = models.ForeignKey(Page, related_name='region_details', on_delete=models.CASCADE)
    offer_region = models.ForeignKey(OfferRegion, related_name='region_details', on_delete=models.CASCADE)
    status = models.CharField(max_length=75)
    local_area_id = models.IntegerField()
    similarity_group = models.CharField(max_length=75, null=True)
    area_similarity = models.FloatField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def __str__(self):
        return "{0}: {1}|{2}".format(self.id, self.page, self.offer_region)



def timedelta_seconds(td):
    return (td.days * 86400) + td.seconds
