import json
import logging
import time
from concurrent.futures.thread import ThreadPoolExecutor

import imageio
import os
from PIL import Image, ImageColor
from PIL import ImageChops

from imagediff.file_storage import get_files_types_from
from .file_storage import get_dirs_from

logger = logging.getLogger(__name__)

MD_IMAGE_DIM = 1000
SM_IMAGE_DIM = 300

executor = ThreadPoolExecutor(max_workers=10)


def rmdir_walk(dir):
    if dir == '/' or 'temp' not in dir:
        print('NO!')
        return
    for root, dirs, files in os.walk(dir, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))
    os.rmdir(dir)

def create_diff(path_one, path_two, diff_path):
    """ Compares to images and saves a diff image, if there is a difference   
    @param: path_one: The path to the first image 
    @param: path_two: The path to the second image """
    try :
        logger.debug('comparing {0} to {1}'.format(path_one, path_two))
        s = time.time()
        timings = []

        # open two originals
        image_one = Image.open(path_one)
        image_two = Image.open(path_two)
        timings.append(time.time() - s)

        # calculate difference
        logger.debug('creating diff {0} to {1}'.format(path_one, path_two))
        diff = ImageChops.difference(image_one, image_two)
        timings.append(time.time() - s)

        # calculate manual difference (DSIM)
        # logger.debug('DSIM {0} to {1}'.format(path_one, path_two))
        # data_one = image_one.getdata()
        # data_two = image_two.getdata()
        # results = [0, 0, 0]
        # for i, item in enumerate(data_one):
        #     if data_one[i] == data_two[i]:
        #         results[0] += 1
        #     elif abs(data_one[i][0] - data_two[i][0]) < 10 and abs(data_one[i][1] - data_two[i][1]) < 10 and abs(data_one[i][2] - data_two[i][2]) < 10:
        #         results[1] += 1
        #     else:
        #         results[2] += 1
        #
        # dsim = (float(results[0]) + (0.5*results[1])) / float((sum(results)))
        # dsim = -1.0

        # logger.info('DIAG loose compare = {0} ({3}) for {1} {2}'.format(results, path_one, path_two, dsim))

        # calculate image hash
        # phash_one = imagehash.phash(image_one)
        # phash_two = imagehash.phash(image_two)
        # dphash = phash_one - phash_two
        # logger.info('DIAG dphash= {0} for {1} {2}'.format(dphash, path_one, path_two))
        #
        # ahash_one = imagehash.average_hash(image_one)
        # ahash_two = imagehash.average_hash(image_two)
        # dahash = ahash_one - ahash_two
        # logger.info('DIAG dahash = {0} for {1} {2}'.format(dahash, path_one, path_two))
        #
        # ic = ImageCompare(image_one, image_two)
        # l = ic.levenshtein
        # logger.info('DIAG levenshtein = {0} for {1} {2}'.format(l, path_one, path_two))
        #
        # fc = FuzzyImageCompare(image_one, image_two)
        # f = fc.similarity()
        # logger.info('DIAG fuzzy = {0} for {1} {2}'.format(f, path_one, path_two))

        # create a diff mask
        logger.debug('masking diff {0} to {1}'.format(path_one, path_two))
        diff_mask = diff.convert("RGBA")
        datas = diff_mask.getdata()
        newData = [(0, 255, 255, 255) for i in range(len(datas))]
        for i, item in enumerate(datas):
            if item[0] == 0 and item[1] == 0 and item[2] == 0:
                newData[i] = (0, 0, 0, 0)
        diff_mask.putdata(newData)
        timings.append(time.time() - s)

        # create a yellow mask
        yellow = Image.new("RGBA", (image_one.width, image_one.height), ImageColor.getrgb('yellow'))
        timings.append(time.time() - s)

        # merge the layers
        overlay = ImageChops.composite(image_two, yellow, diff_mask)
        timings.append(time.time() - s)

        # write smaller images to disk
        temp_file_path = diff_path + '.temp.jpg'
        overlay.resize((SM_IMAGE_DIM, int(SM_IMAGE_DIM / overlay.size[0] * overlay.size[1])), resample=Image.BICUBIC).save(temp_file_path, format='JPEG')
        path_two_sm = "{0}.sm.jpg".format(path_two)
        image_two.resize((SM_IMAGE_DIM, int(SM_IMAGE_DIM / overlay.size[0] * overlay.size[1])), resample=Image.BICUBIC).save(path_two_sm)
        timings.append(time.time() - s)

        i1 = imageio.imread(path_two_sm)
        i2 = imageio.imread(temp_file_path, format='JPEG-PIL')
        images = [i1, i2]
        imageio.mimsave(diff_path, images, loop=0, fps=2)
        timings.append(time.time() - s)

        os.remove(path_two_sm)
        os.remove(temp_file_path)

        logger.debug('done with diff {0} to {1}'.format(path_one, path_two))

        in_dir = os.path.split(path_two)[0]
        in_file = os.path.split(path_two)[-1]
        out_file = os.path.split(diff_path)[-1]
        return {
            "dir": in_dir,
            "orig_fname": in_file,
            "diff_fname" : out_file,
        }
    except:
        logger.exception('failed to compare {0} {1}'.format(path_one, path_two))



def create_diff_async(path_one, path_two, diff_path):
    return executor.submit(create_diff,
                           path_one=path_one,
                           path_two=path_two,
                           diff_path=diff_path
                           )



def create_diff_for(root_path, skip_conversion=False, force_redo=False):
    try:
        logger.debug("compare dirs...")

        originals_dir = os.path.join(root_path, 'originals')
        diff_dir = os.path.join(root_path, 'diffs')
        if force_redo:
            rmdir_walk(diff_dir)
        if not os.path.isdir(diff_dir):
            os.mkdir(diff_dir)

        # get paths to child dirs
        vdirs = get_dirs_from(originals_dir)

        # create diff dirs
        for d in vdirs:
            if not os.path.isdir(os.path.join(diff_dir, d)):
                os.mkdir(os.path.join(diff_dir, d))

        # determine how many page number differences
        version_groups = {
        }
        for v in vdirs:
            num_pages = len(get_files_types_from(os.path.join(originals_dir, v), '.jpg'))
            if not num_pages in version_groups:
                version_groups[num_pages] = []
            version_groups[num_pages].append(v)
        logger.debug('found {0} version-groups'.format(len(version_groups)))

        ret = []
        for k in version_groups:
            logger.debug('processing group {0}'.format(k))
            versions = version_groups[k]

            # set or get the "mother" dir (should change this to cluster)
            mother_dir = versions[0]
            mother_path = os.path.join(originals_dir, mother_dir)
            mfiles = get_files_types_from(mother_path, '.jpg')

            versions.remove(mother_dir)

            # loop through images in mother dir
            s = time.time()
            results = {}
            if not skip_conversion:
                futures = []
                for image in mfiles:
                    m_path = os.path.join(originals_dir, mother_dir, image)
                    # print('processing {0}'.format(m_path))
                    for vdir in versions:
                        c_path = os.path.join(originals_dir, vdir, image)
                        diff_image_path = os.path.join(diff_dir, vdir, image + '.diff.gif')
                        if not os.path.exists(diff_image_path) or force_redo:
                            futures.append(create_diff_async(m_path, c_path, diff_image_path))
                        if not vdir in results:
                            results[vdir] = []
                        results[vdir].append(diff_image_path)


            # also create sm thumbs for mother images
                logger.debug('creating "mother" images...')
                for f in mfiles:
                    image = Image.open(os.path.join(mother_path, f))
                    sm = image.resize((SM_IMAGE_DIM, int(SM_IMAGE_DIM / image.size[0] * image.size[1])), resample=Image.BICUBIC)
                    sm_path = os.path.join(diff_dir, mother_dir, f) + '.diff.jpg'
                    sm.save(sm_path, format='JPEG')

                logger.debug('waiting for futures...')
                # wait for compare operations to finish
                for f in futures:
                    r = f.result(300)

            logger.debug('compare finished in {0}'.format(time.time() - s))
            # print(results)

            # get thumbnail size
            ref_image = Image.open(os.path.join(diff_dir, mother_dir, "{0}.diff.jpg".format(mfiles[0])))

            vg = {
                'name': str(k),
                'img_size': {
                    'h': ref_image.height,
                    'w': ref_image.width,
                },
                'versions': [
                    {
                    'name': mother_dir,
                    'type': 'm',
                    'images': [{'diff_fname':'{0}.diff.jpg'.format(fname)} for fname in mfiles],
                    },
                ]
            }

            for dir in sorted(versions):
                # files = os.listdir(os.path.join(diff_dir, dir))
                # gifs = [file for file in files if (os.path.isfile(os.path.join(root_path, dir, file)) and file.endswith('.gif'))]
                gifs = results[dir]

                vg['versions'].append({
                    'name': dir,
                    'type': 'c',
                    'images': [{'diff_fname':'{0}'.format(fname)} for fname in gifs],
                })

            ret.append(vg)

        with open(os.path.join(diff_dir, 'diffs.json'), 'w') as f:
            f.write(json.dumps(ret))
            f.close()

        return ret
    except:
        logger.exception("compare dir failed")
        return None

def create_diff_for_async(root_path, skip_conversion=False, force_redo=False):
    return executor.submit(create_diff_for,
                           root_path=root_path,
                           skip_conversion=skip_conversion,
                           force_redo=force_redo,
                           )



if __name__ == '__main__':
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)

    ret = create_diff_for('/Users/dan/dev/code/bonial/imagetext/temp/AceHardwareNew-2526557 2.2')

    print(ret)
